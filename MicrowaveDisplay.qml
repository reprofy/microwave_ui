import QtQuick 2.11
import QtQuick.Layouts 1.3


Rectangle {
    function setUpDipslayText (timeToShow) {
        displayText.text = timeToShow
    }
    function hideText ()
    {
        displayText.color = displayRect.color
        textHidden = true
    }
    function unhideText ()
    {
        displayText.color = "lime"
        textHidden = false
    }

    id: displayRect
    property bool textHidden: false
    property string textToDisplay: "00-10"
    property int minutes: 1
    property int seconds: 1
    property date currentDate: new Date()
    property string currentTimeString: currentDate.toLocaleTimeString(Qt.locale(), "hh-mm")
    property bool soundOff: !isSoundOn
    //.toLocaleTimeString(Qt.locale(), "hh-mm")
    property real iconHeight: 5*pxInMm
    height: displayText.height+2*pxInMm + iconHeight//5 mm for the bottom part
    width: globalWidth
    Rectangle {
        id: iconsArea
        width: parent.width
        height: iconHeight + 1*pxInMm
        color: parent.color


        Image {
            id: loudspeakerIcon
            source: "loudspeaker_off.png"
            fillMode: Image.Image.PreserveAspectFit
            anchors.rightMargin: 15*pxInMm
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            height: iconHeight
            visible: soundOff
        }

    }

    FontLoader { id: segmentFont; source: "./Segment7Standard_with__.otf" }
    Text {
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        id: displayText
        font.family: segmentFont.name
        font.pixelSize: pxInMm*20
        // should it be done as a property?
        color: "lime"
        text: currentTimeString
    }
}

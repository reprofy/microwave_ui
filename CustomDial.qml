import QtQuick 2.0

Rectangle {
    id: dial
    property int previousAngleState: 0
    property bool isTaken: false
    property int stateAngle: 10
    property int numOfStates: Math.ceil(360/10)
    property bool wasReleased: false
    Image {
        id: rect
        source: "dial.png"
        anchors.fill: parent
        MouseArea {
            id: dialArea
            anchors.fill: parent
            onReleased: {
                isTaken = false
                console.log(isTaken)
            }
            onPositionChanged:  {
                var point =  mapToItem (dial, mouse.x, mouse.y);
                var diffX = (point.x - rect.width/2);
                var diffY = -1 * (point.y - rect.height/2);
                var rad = Math.atan (diffY / diffX); //suspicious but it works (zero division)
                var deg = (rad * 180 / Math.PI);
                var rotateAngle = 0;

                if (diffX >= 0 && diffY >= 0) {
                    rotateAngle = 90 - Math.abs (deg);
                }
                else if (diffX >= 0 && diffY <= 0) {
                    rotateAngle = 90 + Math.abs (deg);
                }
                else if (diffX <= 0 && diffY >= 0) {
                    rotateAngle = 270 + Math.abs (deg);
                }
                else if (diffX <= 0 && diffY <= 0) {
                    rotateAngle = 270 - Math.abs (deg);
                }
                    //console.log("diffX = ", diffX, "diffY = ", diffY, rotateAngle, previousAngleState)
                    var closestState = Math.ceil(rotateAngle/stateAngle) % numOfStates
                if (!isTaken)
                {
                    previousAngleState = Math.ceil(rotateAngle/stateAngle) % numOfStates
                    isTaken = true
                }

                if (closestState != previousAngleState)
                {
                    rect.rotation += (closestState - previousAngleState)*stateAngle
                    // dirty hacks for dial through 0 transition
                    if (previousAngleState == 0 && closestState == 35)
                    {
                        previousAngleState = 36
                    }
                    if (previousAngleState == 35 && closestState == 0)
                    {
                        closestState = 36
                    }
                    if (previousAngleState == 36 && closestState >= 0 && closestState <= 18)
                    {
                        previousAngleState = 0
                    }
                    dialTurned(closestState - previousAngleState)
                    console.log("Diff state = ", closestState - previousAngleState, closestState, previousAngleState)
                    previousAngleState = closestState
                }
            }


        }
    }
    border.color: "transparent"
    color: "transparent"


}

import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4
import QtQuick.Window 2.3

import QtQml.StateMachine 1.12

import QtQuick.Controls.Styles 1.4

Window {
    function refreshDisplay ()
    {
        if (mainStateClock.active === true)
        {
            mainDisplay.setUpDipslayText(new Date().toLocaleTimeString(Qt.locale(), "hh:mm"))
        } else
        {
            mainDisplay.setUpDipslayText(getTime(Math.floor(cookingTime/60), cookingTime%60))
        }
    }

    Timer {
        id: clockTimer
        interval: 1000
        running: true
        repeat: true
        onTriggered:
        {
            refreshDisplay()
        }

    }
    Timer {
        id: countDownSecondsTimer
        interval: 1000
        running: false
        repeat: true
        onTriggered: {
            cookingTime -= 1
            refreshDisplay()
            if (cookingTime === 0)
            {
                cookingTimeHasExpired()
            }
        }
    }
    Timer {
        id: blinkTimer
        interval: 800
        running: false
        repeat: true

        onTriggered: {
            mainDisplay.textHidden ? mainDisplay.unhideText() : mainDisplay.hideText()
        }
    }

    visible: true
    signal dialTurned (int numberOfDegreesTurned)
    signal cookingTimeHasExpired()
    //    width: 640
    //    height: 480
    title: qsTr("Hello World")
    property int pxInMm: Math.ceil(Screen.pixelDensity)
    property int widthInMn: 80
    property int globalWidth: widthInMn*pxInMm
    property int widthOfButton: 20 * pxInMm
    property int heightOfButton: 10 * pxInMm

    property bool isSoundOn: true

    property int cookingTime: 0 //secs
    property int deltaTime: 5
    property int loudnessLevel: 0  //0 - 3

    property var powerLevels: [200, 450, 700, 800, 1000]
    property int currentPowerLevel_it: powerLevels.length - 1  // index of the current state

    width: pxInMm*90
    height:  pxInMm*180
    function getTime(minutes, seconds) {
        var mins = "0"
        var secs = "0"
        if (minutes <= 9) {
            mins = "0" + minutes.toString()
        } else {
            mins = minutes.toString()
        }
        if (seconds <= 9) {
            secs = "0" + seconds.toString()
        } else {
            secs = seconds.toString()
        }

        var resultingString = mins + ":" + secs
        return resultingString
    }

    StateMachine {
        id: microwaveStateMachine
        initialState: firstState
        running: true
        signal finishedState()
        State {
            id: firstState

            childMode: QState.ParallelStates
            State {
                id: microvaweState
                initialState: mainStateClock
                State {
                    id: mainStateClock
                    SignalTransition {
                        targetState: setUpCookingTime
                        signal: dialTurned
                    }
                    SignalTransition {
                        targetState: setUpPower
                        signal: powerButton.clicked
                    }
                    SignalTransition {
                        targetState: cookingState
                        signal: startButton.clicked
                    }

                    onEntered: {
                        console.log("Main state")
                        cookingTime = 0
                        mainDisplay.setUpDipslayText(new Date().toLocaleTimeString(Qt.locale(), "hh:mm"))
                        clockTimer.start()
                    }
                }

                State {
                    id: setUpCookingTime
                    SignalTransition {
                        targetState: mainStateClock
                        signal: stopButton.clicked
                    }
                    SignalTransition {
                        targetState: cookingState
                        signal: startButton.clicked
                    }

                    onEntered: {
                        console.log("set up cooking time")
                    }
                    onExited: {
                        console.log("exit set up cooking time")
                    }
                }
                State {
                    id: setUpPower
                    SignalTransition {
                        targetState: mainStateClock
                        signal: stopButton.clicked
                    }
                    SignalTransition {
                        targetState: mainStateClock
                        signal: startButton.clicked
                    }
                    SignalTransition {
                        targetState: mainStateClock
                        signal: powerButton.clicked
                    }
                    onEntered: {
                        clockTimer.stop()
                        mainDisplay.setUpDipslayText(powerLevels[currentPowerLevel_it])
                        console.log("setUpPower state")
                    }
                }
                State {
                    id: cookingFinished
                    SignalTransition {
                        targetState: mainStateClock
                        signal: stopButton.clicked
                    }

                    onEntered: {
                        countDownSecondsTimer.stop()
                        cookingTime = 0
                        refreshDisplay()
                        console.log ("Cooking is finished")
                    }
                }

                State {
                    id: pausedCooking
                    SignalTransition {
                        targetState: cookingState
                        signal: startButton.clicked
                    }
                    SignalTransition {
                        targetState: cookingFinished
                        signal: stopButton.clicked
                    }

                    onEntered: {
                        countDownSecondsTimer.stop()
                        mainDisplay.hideText()
                        blinkTimer.start()
                    }
                    onExited: {
                        blinkTimer.stop()
                        mainDisplay.unhideText()
                    }
                }

                State {
                    id: cookingState
                    SignalTransition {
                        targetState: cookingFinished
                        signal: cookingTimeHasExpired
                    }
                    SignalTransition {
                        targetState: pausedCooking
                        signal: stopButton.clicked
                    }

                    onEntered: {
                        console.log("In the cooking state")
                        countDownSecondsTimer.running = true
                    }
                }
            }
            State {
                id: soundState
                initialState: soundOnState
                State {
                    id: soundOnState
                    SignalTransition {
                        targetState: soundOffState
                        //dirty hack: 300 msec should be enough to change loudness
                        signal: soundButton.clicked
                    }
                    onEntered: {
                        isSoundOn = true
                        console.log("Sound is turned ", isSoundOn? "on" : "off")
                        //microwaveStateMachine.finishedState()
                    }
                }
                State {
                    id: soundOffState
                    SignalTransition {
                        targetState: soundOnState
                        //dirty hack: 300 msec should be enough to change loudness
                        signal: soundButton.clicked
                    }
                    onEntered: {
                        isSoundOn = false
                        console.log("Sound is turned ", isSoundOn? "on" : "off")
                        //microwaveStateMachine.finishedState()
                    }
                }
            }
        }
    }

    ColumnLayout{
        anchors.fill: parent
        Layout.topMargin: 5*pxInMm
        MicrowaveDisplay {
            id: mainDisplay
            Layout.alignment: Qt.AlignHCenter
            border.color: "blue"
            border.width: 2
            textToDisplay: "10-10"
            color: "#101010"
        }

        RowLayout {
            width: globalWidth
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: 5*pxInMm
            Button {
                id: powerButton
                //text: "Waves"
                icon.source: "waves.png"
                implicitWidth: widthOfButton
                implicitHeight: heightOfButton
                background: Rectangle {
                    color: powerButton.pressed ? "lightgrey" : "gainsboro"
                    border.color: powerButton.pressed ? "grey" : "darkgrey"
                    border.width: 0.5*pxInMm
                    radius: 0.5*pxInMm
                }
            }
            Button {
                id: menuButton
                text: "Menu"
                implicitWidth: widthOfButton
                implicitHeight: heightOfButton
                background: Rectangle {
                    color: menuButton.pressed ? "lightgrey" : "gainsboro"
                    border.color: menuButton.pressed ? "grey" : "darkgrey"
                    border.width: 0.5*pxInMm
                    radius: 0.5*pxInMm
                }
            }
            Button {
                id: soundButton
                //text: "Sound"
                icon.source: "loudspeaker.png"
                implicitWidth: widthOfButton
                implicitHeight: heightOfButton
                background: Rectangle {
                    color: soundButton.pressed ? "lightgrey" : "gainsboro"
                    border.color: soundButton.pressed ? "grey" : "darkgrey"
                    border.width: 0.5*pxInMm
                    radius: 0.5*pxInMm
                }
            }
        }
        RowLayout {
            width: globalWidth
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: 3*pxInMm
            Button {
                id: startButton
                text: "Start/+30s"
                implicitWidth: widthOfButton * 1.5
                implicitHeight: heightOfButton * 1.4
                background: Rectangle {
                    color: startButton.pressed ? "lightgreen" : "palegreen"
                    border.color: startButton.pressed ? "darkgreen" : "green"
                    border.width: 0.5*pxInMm
                    radius: 1*pxInMm
                }
                onClicked: {
                    if (mainStateClock.active === true || cookingState.active === true)
                    {
                        clockTimer.stop()
                        cookingTime += 30
                        if (cookingTime > 99*60 + 55)
                        {
                            cookingTime = 99*60 + 55
                        }
                        mainDisplay.setUpDipslayText(getTime(Math.floor(cookingTime/60), cookingTime%60))
                    }
                }
            }
            Button {
                id: stopButton
                text: "Stop"
                implicitWidth: widthOfButton * 1.5
                implicitHeight: heightOfButton * 1.4
                background: Rectangle {
                    color: stopButton.pressed ? "salmon" : "lightsalmon"
                    border.color: stopButton.pressed ? "#9b0000" : "darkred"
                    border.width: 0.5*pxInMm
                    radius: 1*pxInMm
                }
            }
        }
        CustomDial {
            id: mainDial
            Layout.alignment: Qt.AlignHCenter
            width: 40*pxInMm
            height: 40*pxInMm
        }
    }
    onDialTurned: {
        clockTimer.stop()
        if (mainStateClock.active === true || setUpCookingTime.active === true
                || cookingState.active === true)
        {
            clockTimer.stop()
            console.log("dial turned", numberOfDegreesTurned)
            if (cookingTime + numberOfDegreesTurned*deltaTime > 0)
            {
                cookingTime = (cookingTime + numberOfDegreesTurned*deltaTime) % (99*60 + 60)
            } else {
                if (cookingTime >0 && numberOfDegreesTurned < 0 && cookingState.active === true)
                {
                    cookingTimeHasExpired()
                    cookingTime = 0
                } else
                    cookingTime = 99*60 + 55
            }
            mainDisplay.setUpDipslayText(getTime(Math.floor(cookingTime/60), cookingTime%60))
        } else if (setUpPower.active === true)
        {
            currentPowerLevel_it = currentPowerLevel_it + numberOfDegreesTurned
            if (currentPowerLevel_it < 0)
                currentPowerLevel_it = 0
            else if (currentPowerLevel_it >= powerLevels.length)
                currentPowerLevel_it = powerLevels.length - 1
            mainDisplay.setUpDipslayText(powerLevels[currentPowerLevel_it])
        }
    }


}
